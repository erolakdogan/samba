import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MenuList } from 'src/models/menuList';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
   httpHeaders = new HttpHeaders()
  .set('Accept', 'application/json');

  constructor(private httpClient: HttpClient) { }

  public getMenu(): Observable<MenuList>{
    return this.httpClient.get<MenuList>("http://localhost:3000/api",{headers: this.httpHeaders});
  }
}

