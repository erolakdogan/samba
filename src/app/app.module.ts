import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AccordionComponent } from './accordion/app.component';
import { mainComponent } from './main/main.component';

@NgModule({
   imports: [
      BrowserModule,
      FormsModule,
      HttpClientModule
   ],
   declarations: [
      AppComponent,
      AccordionComponent,
      mainComponent
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
