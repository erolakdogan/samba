import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../app.service';
import { MenuList } from 'src/models/menuList';

@Component({
  selector: 'accordion',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AccordionComponent implements OnInit {
  config = { multi: false };
  public responseMessage: MenuList;


  constructor(private appService: AppService) {
    this.getMenu();
  }

  public getMenu() {
    this.appService.getMenu().subscribe(response => {

      this.responseMessage = response;

      this.responseMessage.message.menus.forEach(element => {
        element.active = false;
      });
    })
  }

  ngOnInit() {
  }


  toggle(index: number) {
    if (!this.config.multi) {
      this.responseMessage.message.menus.filter(
        (menu, i) => i !== index && menu.active
      ).forEach(menu => menu.active = !menu.active);
    }

    this.responseMessage.message.menus[index].active = !this.responseMessage.message.menus[index].active;
  }
}