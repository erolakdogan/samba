import { Component } from '@angular/core';
import { Config, Menu } from './accordion/types';


@Component({
  selector: 'my-app',
  template: `
    <h1>Accordion Menu</h1>
    <accordion>  </accordion>
  `
})

export class AppComponent  {
 
}

